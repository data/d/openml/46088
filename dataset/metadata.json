{
  "data_set_description": {
    "citation": "https://www.kaggle.com/datasets/darrylljk/london-bus-safety-performance",
    "collection_date": "25-05-2024",
    "contributor": "Darryl",
    "creator": "Darryl",
    "description": "Description:\nThe dataset \"tfl_bus_safety.csv\" provides comprehensive details on bus-related safety incidents reported in London, covering the years 2015 to 2017. It meticulously records incidents by date, bus routes, operating companies, and specifics such as the garage location and the borough where each incident occurred. The diversity of operators and routes highlighted includes Tower Transit, Arriva London South, and several others, reflecting the citywide coverage of the data. Each entry features critical information on the incident, including the injury result, categorizing the severity and treatment following the incident, the type of event leading to the incident, and detailed victim information such as category, sex, and age group. The dataset serves as a valuable resource for analyzing the safety performance of London's bus network over the specified period, identifying potential patterns or areas requiring attention for policy makers, transport operators, and safety analysts alike.\n\nAttribute Description:\n- `year`: The year when the incident occurred (e.g., 2015, 2016, 2017).\n- `date_of_incident`: Specific date of the incident (e.g., 2015-06-01).\n- `route`: Bus route number involved in the incident (e.g., 297, 320).\n- `operator`: Name of the bus operator (e.g., Tower Transit, Arriva London South).\n- `group_name`: Group to which the operator belongs (e.g., London United, Arriva London).\n- `bus_garage`: Location of the bus garage (e.g., Putney, Waterside Way).\n- `borough`: London borough where the incident occurred (e.g., Westminster, Islington).\n- `injury_result_description`: Description of the injury result (e.g., Taken to Hospital - Reported Serious Injury).\n- `incident_event_type`: Type of incident (e.g., Personal Injury, Onboard Injuries).\n- `victim_category`: Category of the victim involved (e.g., Passenger, Cyclist).\n- `victims_sex`: Sex of the victim (e.g., Female, Unknown).\n- `victims_age`: Age category of the victim (e.g., Adult, Unknown).\n\nUse Case:\nThis dataset is instrumental for stakeholders aiming to enhance bus safety across London. Urban planners and transport authorities can leverage the insights to identify high-risk routes or areas, evaluate the effectiveness of various bus operators in managing safety, and tailor interventions to reduce incident rates. Researchers in transportation safety can utilize the data for in-depth analyses of injury outcomes related to bus incidents, contributing to academic and practical knowledge on urban transport safety. Moreover, policy makers can use the dataset to guide regulatory or policy changes, ensuring a safer commuting environment for London's residents and visitors.",
    "description_version": "1",
    "file_id": "22120532",
    "format": "arff",
    "id": "46088",
    "language": "English",
    "licence": "Attribution-ShareAlike (CC BY-SA)",
    "md5_checksum": "e0919800d07ca4691906bc7de947fe2e",
    "minio_url": "https://data.openml.org/datasets/0004/46088/dataset_46088.pq",
    "name": "London_Bus_Safety",
    "parquet_url": "https://data.openml.org/datasets/0004/46088/dataset_46088.pq",
    "processing_date": "2024-05-31 14:45:20",
    "status": "active",
    "upload_date": "2024-05-31T14:44:44",
    "url": "https://api.openml.org/data/v1/download/22120532/London_Bus_Safety.arff",
    "version": "1",
    "visibility": "public"
  }
}