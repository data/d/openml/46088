# OpenML dataset: London_Bus_Safety

https://www.openml.org/d/46088

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Description:
The dataset "tfl_bus_safety.csv" provides comprehensive details on bus-related safety incidents reported in London, covering the years 2015 to 2017. It meticulously records incidents by date, bus routes, operating companies, and specifics such as the garage location and the borough where each incident occurred. The diversity of operators and routes highlighted includes Tower Transit, Arriva London South, and several others, reflecting the citywide coverage of the data. Each entry features critical information on the incident, including the injury result, categorizing the severity and treatment following the incident, the type of event leading to the incident, and detailed victim information such as category, sex, and age group. The dataset serves as a valuable resource for analyzing the safety performance of London's bus network over the specified period, identifying potential patterns or areas requiring attention for policy makers, transport operators, and safety analysts alike.

Attribute Description:
- `year`: The year when the incident occurred (e.g., 2015, 2016, 2017).
- `date_of_incident`: Specific date of the incident (e.g., 2015-06-01).
- `route`: Bus route number involved in the incident (e.g., 297, 320).
- `operator`: Name of the bus operator (e.g., Tower Transit, Arriva London South).
- `group_name`: Group to which the operator belongs (e.g., London United, Arriva London).
- `bus_garage`: Location of the bus garage (e.g., Putney, Waterside Way).
- `borough`: London borough where the incident occurred (e.g., Westminster, Islington).
- `injury_result_description`: Description of the injury result (e.g., Taken to Hospital - Reported Serious Injury).
- `incident_event_type`: Type of incident (e.g., Personal Injury, Onboard Injuries).
- `victim_category`: Category of the victim involved (e.g., Passenger, Cyclist).
- `victims_sex`: Sex of the victim (e.g., Female, Unknown).
- `victims_age`: Age category of the victim (e.g., Adult, Unknown).

Use Case:
This dataset is instrumental for stakeholders aiming to enhance bus safety across London. Urban planners and transport authorities can leverage the insights to identify high-risk routes or areas, evaluate the effectiveness of various bus operators in managing safety, and tailor interventions to reduce incident rates. Researchers in transportation safety can utilize the data for in-depth analyses of injury outcomes related to bus incidents, contributing to academic and practical knowledge on urban transport safety. Moreover, policy makers can use the dataset to guide regulatory or policy changes, ensuring a safer commuting environment for London's residents and visitors.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46088) of an [OpenML dataset](https://www.openml.org/d/46088). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46088/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46088/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46088/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

